package com.kc.platform.model;


import java.io.Serializable;
import java.util.Date;

/**
 * author shish
 * Create Time 2019/3/8 10:56
 * author email shisheng@live.com
 * website www.bangnila.com
 **/
public class ClassTimeEntity implements Serializable {
    private Integer id;
    private  Integer week_num;//周次

    private  String lou;//上课的楼号
    private  Date    start_time;//上课时间
    private  String  longitude;//经度
    private  String  latitude;//维度

    private   Integer course_id;//课程id
    private  Integer uid;//老师id
    private String class_grade;//班级列入151121斑
    private  String course_name;
    private  String teacher_name;
    private Date end_time;//最晚签到时间
    private Double distance ;//最小签到距离

    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }

    public Date getEnd_time() {
        return end_time;
    }

    public void setEnd_time(Date end_time) {
        this.end_time = end_time;
    }

    public String getCourse_name() {
        return course_name;
    }

    public void setCourse_name(String course_name) {
        this.course_name = course_name;
    }

    public String getTeacher_name() {
        return teacher_name;
    }

    public void setTeacher_name(String teacher_name) {
        this.teacher_name = teacher_name;
    }

    public String getClass_grade() {
        return class_grade;
    }

    public void setClass_grade(String class_grade) {
        this.class_grade = class_grade;
    }

    public String getLou() {
        return lou;
    }

    public void setLou(String lou) {
        this.lou = lou;
    }

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getWeek_num() {
        return week_num;
    }

    public void setWeek_num(Integer week_num) {
        this.week_num = week_num;
    }

    public Date getStart_time() {
        return start_time;
    }

    public void setStart_time(Date start_time) {
        this.start_time = start_time;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public Integer getCourse_id() {
        return course_id;
    }

    public void setCourse_id(Integer course_id) {
        this.course_id = course_id;
    }
}
