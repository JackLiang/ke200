package com.kc.platform.apiController;

import com.kc.platform.annotation.LoginUser;
import com.kc.platform.common.R;
import com.kc.platform.model.ClassTimeEntity;
import com.kc.platform.model.SignEntity;
import com.kc.platform.model.User;
import com.kc.platform.service.ApiSignService;
import com.kc.platform.service.ApiUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * author shish
 * Create Time 2019/5/2 16:20
 * author email shisheng@live.com
 * website www.bangnila.com
 **/
@RestController
@RequestMapping("sign")
public class ApiSignController {
    @Autowired
    private ApiUserService apiUserService;
    @Autowired
    private ApiSignService apiSignService;

    @RequestMapping("/get")
    public R getStudentSignList(@LoginUser User user) {
        String grade = user.getClass_grade();//年级
        Date date1 = new Date();
        date1.setHours(0);
        date1.setMinutes(0);
        date1.setSeconds(0);
        Date date2 = new Date();
        date2.setHours(23);
        date2.setMinutes(59);
        date2.setSeconds(59);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        sdf.format(date1);
        sdf.format(date2);
        System.out.println(sdf.format(date1));
        List<ClassTimeEntity> list = apiUserService.QuerySignList(grade, sdf.format(date1), sdf.format(date2));
        Map<String, Object> result = new HashMap<>();
        result.put("list", list);
        return R.ok(result);
    }

    @RequestMapping("/start")
    public R startSign(@LoginUser User user, SignEntity signEntity) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Map<String, Object> result = new HashMap<>();
        Date queryDate = new Date();
        queryDate.setHours(0);
        queryDate.setMinutes(0);
        queryDate.setSeconds(0);
        Date queryDateEnd = new Date();
        queryDateEnd.setHours(23);
        queryDateEnd.setMinutes(59);
        queryDateEnd.setSeconds(59);
        SignEntity sign = apiSignService.querySignList(signEntity.getStu_id(), signEntity.getCourse_id(), sdf.format(queryDate), sdf.format(queryDateEnd));
        if (sign != null) {
            result.put("code", 2000);
            return R.ok(result);
        }
        Date date = new Date();
        signEntity.setSign_time(date);
        Date db_date = sdf.parse(signEntity.getComp_time());
        if (db_date.before(date)) {
            signEntity.setSign_status(0);
            result.put("code", 3000);
        }
        signEntity.setSign_status(1);
        signEntity.setClass_grade(user.getClass_grade());
        Integer code = apiSignService.signSave(signEntity);


        try {
            result.put("code", code);
        } catch (Exception e) {
            code = 1000;
            result.put("code", code);
            e.printStackTrace();
        }
        return R.ok(result);
    }

    @RequestMapping("/total")
    public R total(@LoginUser User user) {
        List<SignEntity> signlist = apiSignService.querylistByUserId(user.getUserId());
        Map<String, Object> result = new HashMap<>();
        result.put("list", signlist);
        return R.ok(result);
    }

    @RequestMapping("/untotal")
    public R untotal(@LoginUser User user) {
        List<SignEntity> signlist = apiSignService.querySignlistByUserId(user.getUserId());
        Map<String, Object> result = new HashMap<>();
        result.put("list", signlist);
        return R.ok(result);
    }

    public static Long dateFormate(String date) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy", Locale.US);
        Date d = sdf.parse(date);
        long ts = d.getTime();
        return ts;
    }
}
