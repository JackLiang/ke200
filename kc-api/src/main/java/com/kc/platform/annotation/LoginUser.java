package com.kc.platform.annotation;

import java.lang.annotation.*;

/**
 * author shish
 * Create Time 2019/1/11 15:25
 * author email shisheng@live.com
 * website www.bangnila.com
 **/
@Target(ElementType.PARAMETER)
@Retention(RetentionPolicy.RUNTIME)
public @interface LoginUser {

}
