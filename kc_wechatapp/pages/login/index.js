Page({

  /**
   * 页面的初始数据
   */

  data: {
    userName: '',
    userPwd: ''
  },

  userNameInput: function(e) {
    var that = this
    that.setData({
      userName: e.detail.value
    })
  },

  userPwdInput: function(e) {
    var that = this
    that.setData({
      userPwd: e.detail.value
    })
  },

  wxlogin: function() {
    var that = this
    var number = 15112111;
    var password = 123456;
    console.log("登陆账户" + number + "登陆密码" + password)
    wx.request({
      url: 'http://localhost:8080/api/login?' + "number=" + number + "&" + "password=" +        password,  
      dataType: "json",
      success: function(res) {
        console.log(res)
        wx.setStorage({
          key: 'token',
          data: res.data.token,
        })
      }
    })
    wx.setStorage({
      key: 'loginName',
      data: '登陆成功',
    })
    wx.getStorage({
      key: 'loginName',
      success: function(res) {
        if (res.data == "登陆成功") {
          wx.showLoading({
            title: '正在登陆',
          })
          setTimeout(function() {
            wx.reLaunch({
              url: '../index/index',
            })
            wx.hideLoading()
          }, 2000)

        } else {
          wx.switchTab({
            url: '../login/index',
          })
        }

      },
      fail: function() {
        wx.switchTab({
          url: '../login/index',
        })
      }
    })



  },



  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    wx.getStorage({
      key: 'loginName',
      success: function(res) {
        console.log("登陆信息", res)
        if (res.data == null || res.data != null) {
          wx.redirectTo({
            url: '../user/user-center/index',
          })
        } else {
          wx.redirectTo({
            url: '../login/index',
          })
        }
      },
    })




  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})