package com.mybatisplus.kc.controller;

import com.mybatisplus.kc.abstractController.AbstractController;
import com.mybatisplus.kc.common.R;
import com.mybatisplus.kc.model.CourseEntity;
import com.mybatisplus.kc.model.DeptCourseEntity;
import com.mybatisplus.kc.model.User;
import com.mybatisplus.kc.service.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * author shish
 * Create Time 2019/3/8 9:44
 * author email shisheng@live.com
 * website www.bangnila.com
 **/
@RestController
@RequestMapping("teacher")
public class ApiTeacherCourseController extends AbstractController {
    @Autowired
    private CourseService courseService;

    @RequestMapping("/add")
    public R addCourse(CourseEntity courseEntity) {
        Map<String, Object> map = new HashMap<>();
        Integer uid=getUserId();
        if (courseEntity.equals("")) {
            return R.error("请输入完整数据");
        }
       try {
           courseEntity.setUid(uid);
           int code = courseService.addCourse(courseEntity);
           map.put("ok", code);
       }catch (Exception e){
           logger.info("出现的错误信息"+e);
           return R.error("系统错误，请稍后再试");
       }
        return R.ok(map);
    }

    @RequestMapping("/del")
    public R delCcourse(Integer id) {
        Map<String, Object> map = new HashMap<>();
        if (id == null) {
            return R.error("请选中你要删除的课程");
        }
        int code = 0;
        try {
            code = courseService.delCourse(id);
            map.put("ok", code);
        } catch (Exception e) {
            logger.info("出现的错误信息"+e);
            return R.error("系统错误，请稍后再试");
        }
        return R.ok(map);
    }

    @RequestMapping("/update")
    public R updateCourse( CourseEntity courseEntity) {
        Map<String, Object> map = new HashMap<>();
        Integer uid=getUserId();
        if (!courseEntity.equals("")) {
            courseEntity.setUid(uid);
            int code = courseService.updateCourse(courseEntity);
            map.put("ok", code);
            return R.ok(map);
        } else {
            return R.error("请输入完整信息");
        }
    }

    @RequestMapping("/select_one")
    public R selectOne( Integer id) {
        Map<String, Object> map = new HashMap<>();
        CourseEntity courseEntity = courseService.selectOne(id);
        map.put("object", courseEntity);
        return R.ok(map);
    }

    @RequestMapping("select_list")
    public R selectCourseList() {
        User user=getUser();
        Map<String, Object> map = new HashMap<>();
        List<DeptCourseEntity> courseEntityList = courseService.selectList(user.getUid());
        map.put("list", courseEntityList);
        return R.ok(map);

    }

}
