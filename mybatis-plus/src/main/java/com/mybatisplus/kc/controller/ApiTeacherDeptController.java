package com.mybatisplus.kc.controller;

import com.mybatisplus.kc.abstractController.AbstractController;
import com.mybatisplus.kc.common.R;
import com.mybatisplus.kc.model.DeptEntity;
import com.mybatisplus.kc.model.User;
import com.mybatisplus.kc.service.DeptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import sun.nio.cs.ext.MacArabic;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * author shish
 * Create Time 2019/3/8 15:22
 * author email shisheng@live.com
 * website www.bangnila.com
 **/
@RestController
@RequestMapping("/dept")
public class ApiTeacherDeptController extends AbstractController {
    @Autowired
    private DeptService deptService;


    //@RequestMapping(value = "add",produces = {"application/json;charset=UTF-8"})
    @RequestMapping("add")
    public R addDept( DeptEntity deptEntity) {
        Map<String, Object> map = new HashMap<>();
        if (deptEntity.equals("")) {
            return R.error("请将数据填写完整");
        }deptEntity.setUid(getUserId());
        int code = deptService.addDept(deptEntity);
        map.put("ok", code);
        return R.ok(map);
    }

    @RequestMapping("del")
    public R delDept(Integer id) {
        Map<String, Object> map = new HashMap<>();
        int code;
        try {
            code = deptService.delDept(id);
            map.put("ok", code);
        } catch (Exception e) {
            return R.error("系统错误，请稍后再试");
        }
        return R.ok(map);
    }

    @RequestMapping("update")
    public R updateDept(DeptEntity deptEntity) {
        Map<String, Object> map = new HashMap<>();
        int code;
        if (deptEntity.equals("")) {
            return R.error("请输入数据");
        }
        try {
            code = deptService.update(deptEntity);
            map.put("ok", code);
        } catch (Exception e) {
            return R.error("系统错误，请稍后再试");
        }
        return R.ok(map);
    }

    @RequestMapping("select_one")
    public R selecOne(Integer id) {
        Map<String, Object> map = new HashMap<>();
        DeptEntity deptEntity = deptService.selectOne(id);
        map.put("rerult", deptEntity);
        return R.ok(map);
    }

    @RequestMapping("select_list")
    public R selectList() {
        User user = getUser();
        Map<String, Object> map = new HashMap<>();
        List<DeptEntity> deptEntityList = deptService.selectDeptList(user.getUid());
        map.put("list", deptEntityList);
        return R.ok(map);

    }


}
