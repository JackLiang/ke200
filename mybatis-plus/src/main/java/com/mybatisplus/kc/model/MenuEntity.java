package com.mybatisplus.kc.model;

import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;

/**
 * author shish
 * Create Time 2019/3/7 14:09
 * author email shisheng@live.com
 * website www.bangnila.com
 **/
@TableName("sys_permission")
public class MenuEntity implements Serializable {

    private  Integer menu_id;
    private  Integer parent_id;
    private  String name;
    private  String permission;
    private  String resource_type;
    private  String url;
    private  String available;


    public Integer getMenu_id() {
        return menu_id;
    }

    public void setMenu_id(Integer menu_id) {
        this.menu_id = menu_id;
    }

    public Integer getParent_id() {
        return parent_id;
    }

    public void setParent_id(Integer parent_id) {
        this.parent_id = parent_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPermission() {
        return permission;
    }

    public void setPermission(String permission) {
        this.permission = permission;
    }

    public String getResource_type() {
        return resource_type;
    }

    public void setResource_type(String resource_type) {
        this.resource_type = resource_type;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getAvailable() {
        return available;
    }

    public void setAvailable(String available) {
        this.available = available;
    }
}
