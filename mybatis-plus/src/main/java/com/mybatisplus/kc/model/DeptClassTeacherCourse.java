package com.mybatisplus.kc.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * author shish
 * Create Time 2019/4/28 14:21
 * author email shisheng@live.com
 * website www.bangnila.com
 **/
@TableName("kc_class_time")
public class DeptClassTeacherCourse implements Serializable {
    @TableId(value="id",type=IdType.AUTO)
    private Integer id;
    @TableField(exist = true)
    private  Integer week_num;//周次
    @TableField(exist = true)
    private  String lou;//上课的楼号
    @TableField(exist = true)
    private String start_time;//上课时间
    @TableField(exist = true)
    private String end_time;//上课时间
    @TableField(exist = true)
    private  String  longitude;//经度
    @TableField(exist = true)
    private  String  latitude;//维度
    @TableField(exist = true)
    private   Integer course_id;//课程id
    @TableField(exist = true)
    private  Integer uid;//老师id
    @TableField(exist = true)
    private String class_grade;//班级列入151121斑
    @TableField(exist = false)
    private String class_name;//班级名字
    @TableField(exist = false)
    private  String dept_name;//学院名字
    @TableField(exist = false)
    private  String teacher_name;//教师名字
    @TableField(exist = false)
    private  String course_name;//课程名称
    @TableField(exist = true)
    private Double distance ;//最小签到距离

    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }

    public String getEnd_time() {
        return end_time;
    }

    public void setEnd_time(String end_time) {
        this.end_time = end_time;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getWeek_num() {
        return week_num;
    }

    public void setWeek_num(Integer week_num) {
        this.week_num = week_num;
    }

    public String getLou() {
        return lou;
    }

    public void setLou(String lou) {
        this.lou = lou;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public Integer getCourse_id() {
        return course_id;
    }

    public void setCourse_id(Integer course_id) {
        this.course_id = course_id;
    }

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public String getClass_grade() {
        return class_grade;
    }

    public void setClass_grade(String class_grade) {
        this.class_grade = class_grade;
    }

    public String getClass_name() {
        return class_name;
    }

    public void setClass_name(String class_name) {
        this.class_name = class_name;
    }

    public String getDept_name() {
        return dept_name;
    }

    public void setDept_name(String dept_name) {
        this.dept_name = dept_name;
    }

    public String getTeacher_name() {
        return teacher_name;
    }

    public void setTeacher_name(String teacher_name) {
        this.teacher_name = teacher_name;
    }

    public String getCourse_name() {
        return course_name;
    }

    public void setCourse_name(String course_name) {
        this.course_name = course_name;
    }
}
