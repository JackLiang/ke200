package com.mybatisplus.kc.model;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;

/**
 * author shish
 * Create Time 2019/3/8 10:43
 * author email shisheng@live.com
 * website www.bangnila.com
 **/
@TableName("kc_course")
public class CourseEntity implements Serializable {
    @TableId(value="id",type= IdType.AUTO)
    private  Integer id;//课程id
    @TableField
    private  String course_name;
    @TableField
    private  String  course_code;//课程编号
    @TableField
    private  Integer dept_id;
    @TableField
    private  Integer uid;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCourse_name() {
        return course_name;
    }

    public void setCourse_name(String course_name) {
        this.course_name = course_name;
    }

    public String getCourse_code() {
        return course_code;
    }

    public void setCourse_code(String course_code) {
        this.course_code = course_code;
    }

    public Integer getDept_id() {
        return dept_id;
    }

    public void setDept_id(Integer dept_id) {
        this.dept_id = dept_id;
    }

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }
}
