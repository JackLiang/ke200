package com.mybatisplus.kc.service.lmp;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

import com.mybatisplus.kc.mapper.MenuMapper;
import com.mybatisplus.kc.mapper.UserMapper;
import com.mybatisplus.kc.model.MenuEntity;
import com.mybatisplus.kc.model.User;
import com.mybatisplus.kc.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * author shish
 * Create Time 2019/1/12 16:35
 * author email shisheng@live.com
 * website www.bangnila.com
 **/
@Service
public class UserServicelmp implements UserService {
  @Autowired
  private UserMapper userMapper;
  @Autowired
  private MenuMapper menuMapper;
    @Override
    public int insertUserInfo(User userInfo) {
        return userMapper.insert(userInfo);
    }

    @Override
    public User findByUserName(String userName) {
      QueryWrapper<User> queryWrapper=new QueryWrapper<>();
     queryWrapper.eq("username",userName);
        return userMapper.selectOne(queryWrapper);
    }

    @Override
    public List<MenuEntity> getMenuByUserId(Integer userId) {
        return menuMapper.getMenuByUserId(userId);
    }
}
