package com.mybatisplus.kc.service.lmp;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.mybatisplus.kc.mapper.CourseMapper;
import com.mybatisplus.kc.model.CourseEntity;
import com.mybatisplus.kc.model.DeptCourseEntity;
import com.mybatisplus.kc.service.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

/**
 * author shish
 * Create Time 2019/3/8 14:36
 * author email shisheng@live.com
 * website www.bangnila.com
 **/
@Service
public class CourseServicelmp implements CourseService {

    @Autowired
    private CourseMapper courseMapper;

    @Override
    public int addCourse(CourseEntity courseEntity) {
        return courseMapper.insert(courseEntity);
    }

    @Override
    public int delCouse(Integer[] ids) {
        return courseMapper.deleteBatchIds(Arrays.asList(ids));
    }

    @Override
    public int delCourse(Integer id) {
        return courseMapper.deleteById(id);
    }
    @Override
    public int updateCourse(CourseEntity courseEntity) {
        return courseMapper.updateById(courseEntity);
    }

    @Override
    public List<DeptCourseEntity> selectList(Integer uid) {
        return courseMapper.selectListByUerId(uid);
    }

    @Override
    public CourseEntity selectOne(Integer id) {
        return courseMapper.selectById(id);
    }
}
