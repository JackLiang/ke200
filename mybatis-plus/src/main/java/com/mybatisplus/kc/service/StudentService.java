package com.mybatisplus.kc.service;

import com.mybatisplus.kc.model.StudentEntity;

import java.util.List;

/**
 * author shish
 * Create Time 2019/4/29 11:06
 * author email shisheng@live.com
 * website www.bangnila.com
 **/
public interface StudentService {

    List<StudentEntity> selectList(String class_grade,Integer uid);
    Integer save(StudentEntity studentEntity);
    StudentEntity selectOne(Long userId);
}
