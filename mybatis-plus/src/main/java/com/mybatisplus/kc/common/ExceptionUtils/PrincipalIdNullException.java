package com.mybatisplus.kc.common.ExceptionUtils;

/**
 * author shish
 * Create Time 2019/1/13 16:24
 * author email shisheng@live.com
 * website www.bangnila.com
 **/
public class PrincipalIdNullException extends RuntimeException   {
    private static final String MESSAGE = "Principal Id shouldn't be null!";

    public PrincipalIdNullException(Class clazz, String idMethodName) {
        super(clazz + " id field: " +  idMethodName + ", value is null\n" + MESSAGE);
    }

}
