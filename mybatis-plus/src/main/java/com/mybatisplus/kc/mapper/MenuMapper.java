package com.mybatisplus.kc.mapper;


import com.mybatisplus.kc.model.MenuEntity;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * author shish
 * Create Time 2019/3/7 14:18
 * author email shisheng@live.com
 * website www.bangnila.com
 **/

public interface MenuMapper {
    @Select("SELECT c.*FROM sys_user_role a LEFT JOIN sys_role_permission b ON a.role_id = b.role_id LEFT JOIN sys_permission c ON b.permission_id = c.menu_id WHERE a.uid = #{userId}")
    List<MenuEntity> getMenuByUserId(Integer userId);
}
