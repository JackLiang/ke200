var vm= new Vue({
        el: '#registerform',
        data: {
            message: 'Hello Vue.js!',
            username:'',
            password:'',
            repassword:'',
            error_message:'',
            captcha:'',
            userInfo:{},
        },
        methods: {
            register:function () {
                if (vm.userInfo.password!==vm.userInfo.repassword){
                    vm.error_message="密码不一致！！请重新输入！！"
                    return
                }
                if (vm.userInfo.password.length<8){
                    vm.error_message="密码长度必须大于八位"
                    return
                }
                console.log(vm.captcha)
                axios({
                    method:'get',
                    url:'../register?'+"captcha="+vm.captcha,
                    params:vm.userInfo,
                }).then(function (resp) {
                    console.log(resp.data);
                    vm.error_message=resp.data.msg
                    if (resp.data.code==101){
                        vm.error_message=resp.data.msg
                    }
                    if (resp.data.code==100) {
                        vm.error_message=resp.data.msg
                        window.location.href='../index';
                    }
                }).catch(function (reason) {
                    console.log('请求失败：'+reason.status+','+reason.statusText);
                })
                console.log(vm.userInfo)
            }
        }
    })
