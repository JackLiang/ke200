
var selectionrow
var selectLength
var vm=new Vue({
    el: '#app',
    data: {
        visible: false,
        down:false,
        columns4: [
            {
                type: 'selection',
                width: 60,
                align: 'center'
            },
            {
                title: '姓名',
                key: 'name'
            },
            {
                title: '学号',
                key: 'userId'
            },
            {
                title: '年级',
                key: 'grade'
            }
        ],
        dataList:[],
        class_grade:"",
        search:"",
        action:'../student/import',
        headers: {
            'Access-Control-Allow-Origin' : '*'
        }

    },
   mounted:function(){
      this.getList()
   },
    methods: {
        handleMaxSize: function (file) {
            alert('文件大小超限,  ' + file.name + ' 太大，上传文件大小不能超过10M.');
        },
        handleFormatError: function (file) {
            alert('文件 ' + file.name + ' 格式不正确，请上传xls格式的文件。');
        },
        handleSuccess: function (res, file) {
            this.getList()
            // 因为上传过程为实例，这里模拟添加 url
            console.log("上传完毕")
            console.log(res)
            alert(res.remark);
        },
        show: function () {

        },
        add:function () {
            this.visible = true;
        },

        update:function () {
            if (selectLength!=1){
                alert("只能选择一条")
            }
        },
        del:function () {

        },
        download:function(){
            this.down=true
        },
        closedown:function(){
            this.down=false
        },
        getList:function () {
            axios({
                method:'post',
                url:"../student/list",
            }).then(function(res){
                vm.dataList=res.data.list
                console.log(res)
        })
        },
        searchBtn:function(){
            console.log("搜索功能")
            console.log(vm.search)
            var  params=vm.search
            console.log(params)
            axios({
                method:'post',
                url:"../student/list?"+"class_grade="+params,
            }).then(function(res){
                console.log(res.data.list)
                vm.dataList=res.data.list

        })
        },
        isSelect:function (selection, row) {
            console.log("选中"+selection.length)
            selectLength=selection.length
            selectionrow=row
            selectionrow
        },
        NoSelect:function (selection, row) {
            console.log("取消"+selection.length)
            selectLength=selection.length
            selectionrow=null;
        },
        all_ok:function (selection) {
            selectLength=0
            console.log("all选中"+selection)
            selectionrow=null;
        },
        all_cancel:function (selection) {
            selectLength=0
            console.log("all取消"+selection)
            selectionrow=null;
        },
        change_status:function(){
            console.log("状态"+vm.dept_id_value)
        },
        reload:function () {
            //页面刷新，目前用的location
            location.reload()
        }
    }
})