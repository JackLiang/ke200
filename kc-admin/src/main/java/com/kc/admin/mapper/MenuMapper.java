package com.kc.admin.mapper;

import com.kc.admin.model.MenuEntity;

import java.util.List;

/**
 * author shish
 * Create Time 2019/3/7 14:18
 * author email shisheng@live.com
 * website www.bangnila.com
 **/

public interface MenuMapper  {
    List<MenuEntity> getMenuByUserId(Integer userId);
}
