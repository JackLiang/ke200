package com.kc.admin.mapper;

import com.kc.admin.model.Permission;
import com.kc.admin.model.Role;
import org.apache.ibatis.annotations.Param;

import java.util.Set;

/**
 * author shish
 * Create Time 2019/1/13 19:37
 * author email shisheng@live.com
 * website www.bangnila.com
 **/

public interface PermissionMapper {
    /**
     * 根据角色查询用户权限
     * @param roles
     * @return
     */
    Set<Permission> findPermissionsByRoleId(@Param("roles") Set<Role> roles);
}
