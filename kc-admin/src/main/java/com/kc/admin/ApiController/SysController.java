package com.kc.admin.ApiController;

import com.kc.admin.common.R;
import com.kc.admin.controller.AbstractController;
import com.kc.admin.model.MenuEntity;
import com.kc.admin.model.User;
import com.kc.admin.service.UserService;
import com.kc.admin.utils.ShiroUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * author shish
 * Create Time 2019/3/7 10:28
 * author email shisheng@live.com
 * website www.bangnila.com
 **/
@RestController
@RequestMapping("sys")
public class SysController extends AbstractController {
   @Autowired
    private UserService userService;
    //根据角色获取到菜单
    @RequestMapping("/menu")
    @RequiresPermissions("sys:admin:select")
    public R SysManager(){
        Integer userId=getUserId();
        List<MenuEntity> menuEntityList=userService.getMenuByUserId(userId);
        Map<String,Object> map =new HashMap<>();
        map.put("list",menuEntityList);
        return R.ok(map);
    }


}
