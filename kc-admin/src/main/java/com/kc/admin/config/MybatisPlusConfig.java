//package com.kc.admin.config;
//
//import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
//import org.mybatis.spring.annotation.MapperScan;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//
///**
// * author shish
// * Create Time 2019/3/7 14:46
// * author email shisheng@live.com
// * website www.bangnila.com
// **/
//@Configuration
//@MapperScan("com.kc.admin.mapper.*")
//public class MybatisPlusConfig {
//    /**
//     * mybatis-plus 分页插件
//     */
//
//    @Bean
//    public PaginationInterceptor paginationInterceptor(){
//        PaginationInterceptor page = new PaginationInterceptor();
//        page.setDialectType("mysql");
//        return page;
//    }
//
//}
